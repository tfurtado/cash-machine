<?php
declare(strict_types=1);

namespace TFurtado\CashMachine;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;

$app       = new App();
$container = $app->getContainer();

$container['note.dispenser'] = function () {
    return new NoteDispenser([Note::fromValue(100), Note::fromValue(50), Note::fromValue(20), Note::fromValue(10)]);
};

$container['withdraw.handler'] = function (ContainerInterface $container) {
    return new WithdrawHandler($container->get('note.dispenser'));
};

$app->post(
    '/withdrawals',
    function (Request $request, Response $response, array $args) {
        /** @var WithdrawHandler $handler */
        $handler = $this->get('withdraw.handler');

        $values = $request->getParsedBody();
        $amount = (int)($values['amount'] ?? 0);

        try {
            $notes = $handler->processWithdraw($amount);

            $response = $response->withStatus(201);
            $response->getBody()->write(json_encode($notes));
        } catch (\InvalidArgumentException $e) {
            $response = $response->withStatus(400);
            $response->getBody()->write(json_encode(['message' => $e->getMessage()]));
        } catch (NoteUnavailableException $e) {
            $response = $response->withStatus(400);
            $response->getBody()->write(
                json_encode(
                    [
                        'message' => 'The requested amount cannot be withdrawn because there are no suitable notes available.',
                    ]
                )
            );
        } finally {
            return $response->withHeader('Content-Type', 'application/json');
        }
    }
);

return $app;
