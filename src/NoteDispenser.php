<?php
declare(strict_types=1);

namespace TFurtado\CashMachine;

class NoteDispenser
{
    /** @var Note[] */
    private $availableNotes;

    /**
     * @param Note[] $availableNotes
     *
     * @throws \UnexpectedValueException
     */
    public function __construct(array $availableNotes)
    {
        $this->availableNotes = [];

        foreach ($availableNotes as $note) {
            if (!$note instanceof Note) {
                throw new \UnexpectedValueException('Only Note instances are accepted.');
            }

            $this->availableNotes[$note->value()] = $note;
        }

        krsort($this->availableNotes);
    }

    /**
     * @param int $amount The amount to be withdrawn
     *
     * @return Note[]
     * @throws \InvalidArgumentException
     * @throws NoteUnavailableException
     */
    public function withdraw(int $amount): array
    {
        if ($amount < 0) {
            throw new \InvalidArgumentException('Amount must be a non-negative number');
        }

        $notes = [];

        while ($amount > 0) {
            $selectedNote = null;

            foreach ($this->availableNotes as $note) {
                if ($note->value() <= $amount) {
                    $selectedNote = $note;
                    break;
                }
            }

            if ($selectedNote === null) {
                throw new NoteUnavailableException();
            }

            $amount  -= $selectedNote->value();
            $notes[] = $selectedNote;
        }

        return $notes;
    }
}
