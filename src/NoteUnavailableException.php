<?php
declare(strict_types=1);

namespace TFurtado\CashMachine;

class NoteUnavailableException extends \RuntimeException
{
}
