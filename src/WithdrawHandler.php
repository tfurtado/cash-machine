<?php
declare(strict_types=1);

namespace TFurtado\CashMachine;

class WithdrawHandler
{
    /** @var NoteDispenser */
    private $dispenser;

    public function __construct(NoteDispenser $dispenser)
    {
        $this->dispenser = $dispenser;
    }

    /**
     * @param int $amount
     *
     * @return Note[]
     *
     * @throws NoteUnavailableException
     * @throws \InvalidArgumentException
     */
    public function processWithdraw(int $amount)
    {
        $notes = [];
        foreach ($this->dispenser->withdraw($amount) as $note) {
            $notes[] = $note->value();
        }

        return $notes;
    }
}
