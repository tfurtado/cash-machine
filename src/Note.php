<?php
declare(strict_types=1);

namespace TFurtado\CashMachine;

class Note
{
    /** @var int */
    private $value;

    private function __construct(int $value)
    {
        $this->value = $value;
    }

    public static function fromValue(int $value)
    {
        return new self($value);
    }

    public function value(): int
    {
        return $this->value;
    }
}
