<?php
declare(strict_types=1);

namespace TFurtado\CashMachine;

use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \TFurtado\CashMachine\NoteDispenser
 */
class NoteDispenserTest extends TestCase
{
    /**
     * @test
     * @expectedException \UnexpectedValueException
     * @covers ::__construct
     */
    public function ensureTheConstructorAcceptsOnlyNoteInstances()
    {
        new NoteDispenser([100]);
    }

    /**
     * @test
     * @expectedException \TFurtado\CashMachine\NoteUnavailableException
     * @covers ::__construct
     * @covers ::withdraw
     */
    public function anExceptionIsThrownIfItIsNotPossibleToWithdrawTheGivenAmount()
    {
        $this->aDispenser()->withdraw(125);
    }

    /**
     * @test
     * @covers ::__construct
     * @covers ::withdraw
     */
    public function anEmptySetIsReturnedIfThereIsNoAmountToBeWithdrawn()
    {
        $this->assertCount(0, $this->aDispenser()->withdraw(0));
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     * @covers ::__construct
     * @covers ::withdraw
     */
    public function anExceptionIsThrownIfTheGivenAmountIsNegative()
    {
        $this->aDispenser()->withdraw(-130);
    }

    /**
     * @param int    $amount
     * @param Note[] $notes
     *
     * @test
     * @covers ::__construct
     * @covers ::withdraw
     * @dataProvider provideValidWithdraws
     */
    public function ensureTheRightSetOfNotesIsReturned(int $amount, array $notes)
    {
        $this->assertEquals($notes, $this->aDispenser()->withdraw($amount));
    }

    public function provideValidWithdraws()
    {
        return [
            [30, [Note::fromValue(20), Note::fromValue(10)]],
            [80, [Note::fromValue(50), Note::fromValue(20), Note::fromValue(10)]],
            [
                280,
                [
                    Note::fromValue(100),
                    Note::fromValue(100),
                    Note::fromValue(50),
                    Note::fromValue(20),
                    Note::fromValue(10),
                ],
            ],
        ];
    }

    private function aDispenser(): NoteDispenser
    {
        $dispenser = new NoteDispenser(
            [Note::fromValue(100), Note::fromValue(50), Note::fromValue(20), Note::fromValue(10)]
        );

        return $dispenser;
    }
}
