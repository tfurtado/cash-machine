<?php
declare(strict_types=1);

namespace TFurtado\CashMachine;

require __DIR__ . '/../vendor/autoload.php';

/** @var \Slim\App $app */
$app = require __DIR__ . '/../app/app.php';
$app->run();
